from django.db import models
from django.contrib.auth.models import User
from taggit.managers import TaggableManager
class Profile(models.Model):
	user = models.OneToOneField(User)
	address = models.TextField(blank=True)
	phoneNumber = models.CharField(blank=True, max_length=80)
GENDER=((1,"MALE"),(2,"FEMALE"))
BLOODTYPE=((1,"A"),(2,"B"),(3,"AB"),(4,"O"))	
class Patient(models.Model):
	firstName = models.CharField(blank=True, max_length=80)
	lastName = models.CharField(blank=False, max_length=80)
	dateOfBirth = models.DateField(blank=False, null=True, auto_now_add=True)
	physicalAddress = models.CharField(blank=True, max_length=80,null=True)
	phoneNumber = models.CharField(blank=False, max_length=80)
	alternatePhoneNumber = models.CharField(blank=True, max_length=80, null=True)
	maritalStatus = models.CharField(blank=True, max_length=80)
	emailAddress = models.EmailField()
	created = models.DateTimeField(blank=True, null=True, auto_now_add=True)
	photo = models.FileField(upload_to="pats", null=True)
	sex = models.IntegerField(choices=GENDER)
	bloodType = models.IntegerField( null=True,choices=BLOODTYPE)
	height = models.FloatField()
	weight = models.FloatField()
	alergies = models.TextField(blank=True)
	nextOfKinContact = models.TextField(blank=True)
	tags = TaggableManager()
	def __unicode__(self):
		return self.firstName +" " +self.lastName
	def interviews(self):
		return PatientInterview.objects.filter(patient__id=self.id)
class PatientInterview(models.Model):
	patient = models.OneToOneField(Patient)
	created = models.DateTimeField(blank=True, null=True, auto_now_add=True)
	problemPresentedByPatient = models.TextField(blank=True, null=True)
	observations = models.TextField(blank=True, null=True)
	examinations = models.TextField(blank=True, null=True)
	diagnosis = models.TextField(blank=True, null=True)
	treatmentGiven = models.TextField(blank=True, null=True)
	extraRemarks = models.TextField(blank=True, null=True)
	def __unicode__(self):
		return self.patient.firstName +" " +self.patient.lastName +" on "+ str(self.created.date())
	
class ObservationFile(models.Model):
	patientInterview = models.OneToOneField(PatientInterview)
	file = models.FileField(upload_to="pats")
	fileType = models.CharField(blank=True, max_length=80)
	created = models.DateTimeField(blank=True, null=True, auto_now_add=True)