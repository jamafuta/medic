#crudgenerator auto-generated code.
#crudgenetaror date: 11th August 2012 11:11
from django.core.urlresolvers import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView , DetailView
from models import Patient

class PatientDetailView(DetailView):
    model=Patient
class PatientListView(ListView):
    model=Patient
    paginate_by=20

class PatientDeleteView(DeleteView):
    model=Patient
    def get_success_url(self):
        return reverse("patdb:patient:list", args=(1,))

class PatientCreateView(CreateView):
    model=Patient
    def get_success_url(self):
        return reverse("patdb:patient:list", args=(1,))

class PatientUpdateView(UpdateView):
    model=Patient
    def get_success_url(self):
        return reverse("patdb:patient:list", args=(1,))
#crudgenerator auto-generated code.
#crudgenetaror date: 11th August 2012 11:12
from django.core.urlresolvers import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from models import PatientInterview


class PatientInterviewListView(ListView):
    model=PatientInterview
    paginate_by=20
class PatientInterviewDetailView(DetailView):
    model=PatientInterview
class PatientInterviewDeleteView(DeleteView):
    model=PatientInterview
    def get_success_url(self):
        return reverse("patdb:patientinterview:list", args=(1,))

class PatientInterviewCreateView(CreateView):
    model=PatientInterview
    def get_success_url(self):
        return reverse("patdb:patientinterview:list", args=(1,))

class PatientInterviewUpdateView(UpdateView):
    model=PatientInterview
    def get_success_url(self):
        return reverse("patdb:patientinterview:list", args=(1,))
#crudgenerator auto-generated code.
#crudgenetaror date: 11th August 2012 11:12
from django.core.urlresolvers import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from models import ObservationFile


class ObservationFileListView(ListView):
    model=ObservationFile
    paginate_by=20

class ObservationFileDeleteView(DeleteView):
    model=ObservationFile
    def get_success_url(self):
        return reverse("patdb:observationfile:list", args=(1,))

class ObservationFileCreateView(CreateView):
    model=ObservationFile
    def get_success_url(self):
        return reverse("patdb:observationfile:list", args=(1,))

class ObservationFileUpdateView(UpdateView):
    model=ObservationFile
    def get_success_url(self):
        return reverse("patdb:observationfile:list", args=(1,))
