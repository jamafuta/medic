from django.conf.urls.defaults import *
from views import PatientListView, PatientUpdateView, PatientCreateView, PatientDeleteView , PatientDetailView

patient_patterns= patterns('',
    url(r'^list/page(?P<page>[0-9]+)/(\?.*)?$', 
        PatientListView.as_view(),
        name='list'),
    url(r'^update/(?P<pk>\d+)/$', 
        PatientUpdateView.as_view(),
        name='update'),
    url(r'^create/$', 
        PatientCreateView.as_view(),
        name='create'),
    url(r'^delete/(?P<pk>[0-9]+)/$', 
        PatientDeleteView.as_view(),
        name='delete'),
    url(r'^view/(?P<pk>[0-9]+)/$',PatientDetailView.as_view(),name="view"),
)

newpatterns = patterns('',
    url(r'^patient/', include(patient_patterns, namespace="patient")),
)

try:
    urlpatterns+=newpatterns
except NameError:
    urlpatterns=newpatterns
from django.conf.urls.defaults import *
from views import PatientInterviewListView, PatientInterviewUpdateView, PatientInterviewCreateView, PatientInterviewDeleteView ,PatientInterviewDetailView

patientinterview_patterns= patterns('',
    url(r'^list/page(?P<page>[0-9]+)/(\?.*)?$', 
        PatientInterviewListView.as_view(),
        name='list'),
    url(r'^update/(?P<pk>\d+)/$', 
        PatientInterviewUpdateView.as_view(),
        name='update'),
    url(r'^create/$', 
        PatientInterviewCreateView.as_view(),
        name='create'),
    url(r'^delete/(?P<pk>[0-9]+)/$', 
        PatientInterviewDeleteView.as_view(),
        name='delete'),
     url(r'^view/(?P<pk>[0-9]+)/$',PatientInterviewDetailView.as_view(),name="view"),
)

newpatterns = patterns('',
    url(r'^patientinterview/', include(patientinterview_patterns, namespace="patientinterview")),
)

try:
    urlpatterns+=newpatterns
except NameError:
    urlpatterns=newpatterns
from django.conf.urls.defaults import *
from views import ObservationFileListView, ObservationFileUpdateView, ObservationFileCreateView, ObservationFileDeleteView

observationfile_patterns= patterns('',
    url(r'^list/page(?P<page>[0-9]+)/(\?.*)?$', 
        ObservationFileListView.as_view(),
        name='list'),
    url(r'^update/(?P<pk>\d+)/$', 
        ObservationFileUpdateView.as_view(),
        name='update'),
    url(r'^create/$', 
        ObservationFileCreateView.as_view(),
        name='create'),
    url(r'^delete/(?P<pk>[0-9]+)/$', 
        ObservationFileDeleteView.as_view(),
        name='delete'),
)

newpatterns = patterns('',
    url(r'^observationfile/', include(observationfile_patterns, namespace="observationfile")),
)

try:
    urlpatterns+=newpatterns
except NameError:
    urlpatterns=newpatterns
