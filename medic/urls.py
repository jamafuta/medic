from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf.urls.static import static
admin.autodiscover()
import settings
urlpatterns = patterns('',
    url(r'^$', 'medic.views.home', name='home'),
    # url(r'^medic/', include('medic.foo.urls')),
    url(r'^tags/(?P<pk>[0-9]+)/$',"medic.views.tags",name="patient_tag"),
    url(r'^patdb/', include('patdb.urls',namespace='patdb')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
	url(r'^accounts/', include('registration.backends.default.urls')),
	url(r'^login$', 'django.contrib.auth.views.login', {'template_name': 'login.html'},name="login"),
	url(r'^logout$', 'django.contrib.auth.views.logout', {'template_name': 'logout.html'},name="logout"),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
