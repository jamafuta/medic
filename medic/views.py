from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from taggit.models import Tag
from patdb.models import Patient
def home(request):
	return render_to_response( 'index.html', {}, context_instance=RequestContext(request))
def tags(request,pk):
	tag = Tag.objects.get(pk=pk)
	patients =Patient.objects.filter(tags__pk=pk)
	return render_to_response( 'tagged.html', {"tag":tag,"patients":patients}, context_instance=RequestContext(request))